/*!
    \page Adding-a-concept-based-node
    \title How to Add a node based on a concept
    \ingroup HowTo
    
    The process is, for the most part, the same as described in \l{Adding-a-standalone-node}{How to add a standalone composer node}. This page focuses on the few differences. The main change is a combobox allows to choose which implementation the node should use.
        
    \section1 Difference with the standalone node
    
    On the three files that are required for a node, there are only a few changes to be made. The json file has exactly the same structure, and registering the node is done exactly in the same way.
    
    \section2 Header's changes
    
    A concept-based node class derives from the template class dtkComposerNodeObject, templated by the concept, instead of inheriting from dtkComposerNodeLeaf. This imply that the concept's header has to be included.
    
    \section2 implementation changes
    
    The node constructor now has to query all the available implementations of the concept. This is done by adding a call to \c setFactory() which takes as parameter the plugin factory associated to the concept (type-checking is performed through the template parameter of the super class).
    
    In the \c run() method, the implementation chosen by the user is available through the method \c object(). The only pitfall is this method can return a NULL pointer. Hence the result has to be checked before being used.
        
    \section1 Full example
    
    The following sections show the three files used to implements the dtkStatisticsNode.
    
    \section2 json file
    
    \code
    {
              "title" : "Statistics",
               "kind" : "process",
               "type" : "StatisticsFilter",
               "tags" : ["statistics"],
        "description" : "<h3>Statistics</h3>
                         <p>Statistics on an image </p>",
             "inputs" : ["image"],
             "outputs": ["mean","sigma","variance","sum","min","max"]
    }
    \endcode
    
    \section2 header file
    
    \code
    #pragma once

    #include <dtkComposer>

    #include "dtkImage.h"
    #include "dtkAbstractStatisticsCalculator.h"

    class dtkStatisticsNodePrivate;

    // ///////////////////////////////////////////////////////////////////
    //
    // ///////////////////////////////////////////////////////////////////

    class dtkStatisticsNode : public dtkComposerNodeObject<dtkAbstractStatisticsCalculator>
    {
    public:
         dtkStatisticsNode(void);
        ~dtkStatisticsNode(void);

    public:
        void run(void);

    private:
        dtkStatisticsNodePrivate *d;
    };

    //
    // dtkStatisticsNode.h ends here
    \endcode
    
    \section2 source file
    
    \code

    #include <dtkComposer>

    #include "dtkStatisticsNode.h"
    #include "dtkImaging.h"

    class dtkStatisticsNodePrivate
    {
    public:
        dtkComposerTransmitterReceiver< dtkImage* > imgRecv;
        
        dtkComposerTransmitterEmitter< double >     meanEmt;
        dtkComposerTransmitterEmitter< double >     sigmaEmt;
        dtkComposerTransmitterEmitter< double >     varianceEmt;
        dtkComposerTransmitterEmitter< double >     sumEmt;

        dtkComposerTransmitterEmitter< double >     minEmt;
        dtkComposerTransmitterEmitter< double >     maxEmt;
    };

    dtkStatisticsNode::dtkStatisticsNode(void) : dtkComposerNodeObject<dtkAbstractStatisticsCalculator>(), d(new dtkStatisticsNodePrivate())
    {
        this->setFactory(dtkImaging::statistics::pluginFactory());
        this->appendReceiver(&d->imgRecv);
        
        this->appendEmitter(&d->meanEmt);
        this->appendEmitter(&d->sigmaEmt);
        this->appendEmitter(&d->varianceEmt);
        this->appendEmitter(&d->sumEmt);

        this->appendEmitter(&d->minEmt);
        this->appendEmitter(&d->maxEmt);
    }

    dtkStatisticsNode::~dtkStatisticsNode(void)
    {
        delete d;
    }

    void dtkStatisticsNode::run(void)
    {
        if (d->imgRecv.isEmpty())
        {
            qDebug() << Q_FUNC_INFO << "The input is not set. Aborting.";
            return;
        }
        else
        {
            dtkAbstractStatisticsCalculator* statistics = this->object();
            if(!this->object())
                return;
            statistics->setImage(d->imgRecv.data());
            statistics->run();

            d->meanEmt.setData(statistics->mean());
            d->sigmaEmt.setData(statistics->sigma());
            d->varianceEmt.setData(statistics->variance());
            d->sumEmt.setData(statistics->sum());

            d->minEmt.setData(statistics->min());
            d->maxEmt.setData(statistics->max());
        }
    }
    \endcode

*/
