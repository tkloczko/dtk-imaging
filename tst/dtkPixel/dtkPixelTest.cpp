// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkPixelTest.h"
#include "dtkPixelImpl.h"

#include <dtkImagingCore>

#include <QDebug>

void dtkPixelTestCase::initTestCase(void)
{

}

void dtkPixelTestCase::init(void)
{

}

void dtkPixelTestCase::testScalarPixel(void)
{
    dtkScalarPixel<double> *pix = new dtkScalarPixel<double>;

    QVERIFY(QString(QMetaType::typeName(pix->pixelType())) == "double");
    QVERIFY(pix->pixelDim() == 1);
    QVERIFY(pix->identifier() == "Scalar");
}

void dtkPixelTestCase::testRGBPixel(void)
{
    dtkRGBPixel<double> *pix = new dtkRGBPixel<double>;

    QVERIFY(QString(QMetaType::typeName(pix->pixelType())) == "double");
    QVERIFY(pix->pixelDim() == 3);
    QVERIFY(pix->identifier() == "RGB");
}

void dtkPixelTestCase::testCompileFailure(void)
{

}

void dtkPixelTestCase::cleanupTestCase(void)
{

}

void dtkPixelTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkPixelTest, dtkPixelTestCase)

//
// dtkPixelTest.cpp ends here
