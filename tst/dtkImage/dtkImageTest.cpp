// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageTest.h"

#include <dtkImagingCore>
#include <dtkImagingWidgets>

void dtkImageTestCase::initTestCase(void)
{
    dtkImagingSettings settings;
    settings.beginGroup("imaging");
    dtkImaging::initialize(settings.value("plugins").toString());
    settings.endGroup();
}

void dtkImageTestCase::init(void)
{

}

void dtkImageTestCase::testCopy(void)
{
    QString sourceDataPath = "../minimal.nii";
    QTemporaryFile file("dtk-imaging-XXXXXX.nii");
    if (file.open()) {
        dtkImage *img = dtkImaging::read(sourceDataPath);

        if (img) {
            qDebug() << Q_FUNC_INFO << img->extent() << img;
            bool ret = dtkImaging::write(file.fileName(), img);
            QCOMPARE(ret, true);
        }

    } else {
        QFAIL("Can't create temporary file!");
    }
}

void dtkImageTestCase::testView(void)
{
    QString sourceDataPath = "../minimal.nii";

    dtkImage *img = dtkImaging::read(sourceDataPath);
    //QVERIFY(img != NULL);

    if (img) {
        qDebug() << img->identifier() << img->dim() << img->rawData();

        dtkImaging::display(img);
    }
}

void dtkImageTestCase::cleanupTestCase(void)
{
    dtkImaging::uninitialize();
}

void dtkImageTestCase::cleanup(void)
{

}

DTKTEST_MAIN(dtkImageTest, dtkImageTestCase)

//
// dtkImageTest.cpp ends here
