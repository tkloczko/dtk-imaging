## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

add_subdirectory(dtkImagingCore)
add_subdirectory(dtkImagingFilters)

if(DTK_IMAGING_BUILD_WIDGETS)
  add_subdirectory(dtkImagingWidgets)
endif(DTK_IMAGING_BUILD_WIDGETS)

if(DTK_IMAGING_BUILD_COMPOSER)
  add_subdirectory(dtkImagingComposer)
endif(DTK_IMAGING_BUILD_COMPOSER)

## #################################################################
## Targets
## #################################################################

set(DTKIMAGING_TARGETS)

set(DTKIMAGING_TARGETS ${DTKIMAGING_TARGETS} dtkImagingCore)
set(DTKIMAGING_TARGETS ${DTKIMAGING_TARGETS} dtkImagingFilters)

if(DTK_IMAGING_BUILD_WIDGETS)
  set(DTKIMAGING_TARGETS ${DTKIMAGING_TARGETS} dtkImagingWidgets)
endif(DTK_IMAGING_BUILD_WIDGETS)

if(DTK_IMAGING_BUILD_COMPOSER)
  set(DTKIMAGING_TARGETS ${DTKIMAGING_TARGETS} dtkImagingComposer)
endif(DTK_IMAGING_BUILD_COMPOSER)

## #################################################################
## Target export
## #################################################################

export(TARGETS ${DTKIMAGING_TARGETS} FILE "${CMAKE_BINARY_DIR}/dtkImagingDepends.cmake")

######################################################################
### CMakeLists.txt ends here
