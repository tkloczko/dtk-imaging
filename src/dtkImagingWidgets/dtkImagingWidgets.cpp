// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImagingWidgets.h"

#include "dtkAbstractImageViewer.h"

// /////////////////////////////////////////////////////////////////
// Display image
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {

    void display(dtkImage *image)
    {
        QStringList viewers = viewer::pluginFactory().keys();

        if (viewers.isEmpty()) {
            dtkError() << Q_FUNC_INFO << "No viewer has been found. Noting is done.";
            return;
        }

        dtkAbstractImageViewer *viewer = nullptr;

        QString viewer_name = "vtkImageViewer";

        if (!viewers.contains(viewer_name)) {
            viewer_name = viewers.front();
        }

        viewer = viewer::pluginFactory().create(viewer_name);

        if (viewer) {
            viewer->display(image);
        } else {
            dtkError() << Q_FUNC_INFO << viewer_name << "cannot be instanciated. Noting is done.";
        }
    }
}

//
// dtkImagingWidgets.cpp ends here
