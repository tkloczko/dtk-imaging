// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractAddFilter;
class dtkAddFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkAddFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkAddFilterNode : public dtkComposerNodeObject<dtkAbstractAddFilter>
{
public:
     dtkAddFilterNode(void);
    ~dtkAddFilterNode(void);

public:
    void run(void);

private:
    dtkAddFilterNodePrivate *d;
};

//
// dtkAddFilterNode.h ends here
