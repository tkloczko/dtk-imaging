// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkFSLInitNode.h"

#include <dtkLog>

Q_DECLARE_METATYPE(QProcessEnvironment);

// /////////////////////////////////////////////////////////////////
// dtkFSLInitNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkFSLInitNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString>            pathRecv;
    dtkComposerTransmitterEmitter<QProcessEnvironment> envEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkFSLInitNode
// /////////////////////////////////////////////////////////////////

dtkFSLInitNode::dtkFSLInitNode(void) : d(new dtkFSLInitNodePrivate())
{
    this->appendReceiver(&d->pathRecv);
    this->appendEmitter(&d->envEmt);
}

dtkFSLInitNode::~dtkFSLInitNode(void)
{
    delete d;
}

void dtkFSLInitNode::run(void)
{
    if (d->pathRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        QString path = d->pathRecv.data();
        if (!QFile::exists(path)) {
            dtkError() << Q_FUNC_INFO << path << "does not exists";
            return;
        }
        QDir::setCurrent(path);
        QProcessEnvironment pe = QProcessEnvironment::systemEnvironment();
        pe.insert("FSLDIR",path);
        pe.insert("FSLOUTPUTTYPE","NIFTI_GZ");
        pe.insert("FSLTCLSH",path+"/bin/fsltclsh");
        pe.insert("FSLWISH",path+"/bin/fslwish");
        pe.insert("FSLMULTIFILEQUIT","TRUE");

        d->envEmt.setData(pe);
    }
}

//
// dtkFSLInitNode.cpp ends here
