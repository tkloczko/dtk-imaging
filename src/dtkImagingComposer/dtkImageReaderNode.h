// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkImageReader;
class dtkImageReaderNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkImageReaderNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageReaderNode : public dtkComposerNodeObject<dtkImageReader>
{
public:
     dtkImageReaderNode(void);
    ~dtkImageReaderNode(void);

public:
    void run(void);

private:
    dtkImageReaderNodePrivate *d;
};

//
// dtkImageReaderNode.h ends here
