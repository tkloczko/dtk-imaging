// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractCloseFilter;
class dtkCloseFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkCloseFilterNode : public dtkComposerNodeObject<dtkAbstractCloseFilter>
{
public:
     dtkCloseFilterNode(void);
    ~dtkCloseFilterNode(void);

public:
    void run(void);

private:
    dtkCloseFilterNodePrivate *d;
};

//
// dtkCloseFilterNode.h ends here
