// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDivideFilterNode.h"

#include "dtkAbstractDivideFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkDivideFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkDivideFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkDivideFilterNode
// /////////////////////////////////////////////////////////////////

dtkDivideFilterNode::dtkDivideFilterNode(void) : dtkComposerNodeObject<dtkAbstractDivideFilter>(), d(new dtkDivideFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::divide::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkDivideFilterNode::~dtkDivideFilterNode(void)
{
    delete d;
}

void dtkDivideFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractDivideFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Divide filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->setValue(d->radiusRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkDivideFilterNode.cpp ends here
