// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageMeshWriterNode.h"

#include "dtkImageMeshWriter.h"
#include "dtkImageMesh.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImageMeshWriterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkImageMeshWriterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString>        pathRecv;
    dtkComposerTransmitterReceiver<dtkImageMesh *> meshRecv;
};

// /////////////////////////////////////////////////////////////////
// dtkImageMeshWriterNode
// /////////////////////////////////////////////////////////////////

dtkImageMeshWriterNode::dtkImageMeshWriterNode(void) : d(new dtkImageMeshWriterNodePrivate())
{
    this->setFactory(dtkImaging::meshWriter::pluginFactory());

    this->appendReceiver(&d->pathRecv);
    this->appendReceiver(&d->meshRecv);
}

dtkImageMeshWriterNode::~dtkImageMeshWriterNode(void)
{
    delete d;
}

void dtkImageMeshWriterNode::run(void)
{
    if (d->pathRecv.isEmpty() || d->meshRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkImageMeshWriter *writer = this->object();
        if (!writer) {
            dtkError() << Q_FUNC_INFO << "No Mesh writer found. Aborting.";
            return;
        }

        writer->setMesh(d->meshRecv.constData());
        writer->setPath(d->pathRecv.constData());
        writer->write();
    }
}

//
// dtkImageMeshWriterNode.cpp ends here
