// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractImageSubtraction;
class dtkSubtractionImageNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkSubtractionImageNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkSubtractionImageNode : public dtkComposerNodeObject<dtkAbstractImageSubtraction>
{
public:
     dtkSubtractionImageNode(void);
    ~dtkSubtractionImageNode(void);

public:
    void run(void);

private:
    dtkSubtractionImageNodePrivate *d;
};

//
// dtkSubtractionImageNode.h ends here
