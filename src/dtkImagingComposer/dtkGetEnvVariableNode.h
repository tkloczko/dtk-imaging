// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkGetEnvVariableNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkGetEnvVariableNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkGetEnvVariableNode : public dtkComposerNodeLeaf
{
public:
     dtkGetEnvVariableNode(void);
    ~dtkGetEnvVariableNode(void);

public:
    void run(void);

private:
    dtkGetEnvVariableNodePrivate *d;
};

//
// dtkGetEnvVariableNode.h ends here
