// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkInvertFilterNode.h"

#include "dtkAbstractInvertFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkInvertFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkInvertFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;
    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkInvertFilterNode
// /////////////////////////////////////////////////////////////////

dtkInvertFilterNode::dtkInvertFilterNode(void) : dtkComposerNodeObject<dtkAbstractInvertFilter>(), d(new dtkInvertFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::invert::pluginFactory());

    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkInvertFilterNode::~dtkInvertFilterNode(void)
{
    delete d;
}

void dtkInvertFilterNode::run(void)
{
    if (d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractInvertFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Invert filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkInvertFilterNode.cpp ends here
