// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractOpenFilter;
class dtkOpenFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkOpenFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkOpenFilterNode : public dtkComposerNodeObject<dtkAbstractOpenFilter>
{
public:
     dtkOpenFilterNode(void);
    ~dtkOpenFilterNode(void);

public:
    void run(void);

private:
    dtkOpenFilterNodePrivate *d;
};

//
// dtkOpenFilterNode.h ends here
