// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkRobexNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkRobexNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkRobexNode : public dtkComposerNodeLeaf
{
public:
     dtkRobexNode(void);
    ~dtkRobexNode(void);

public:
    void run(void);

private:
    dtkRobexNodePrivate *d;
};

//
// dtkRobexNode.h ends here
