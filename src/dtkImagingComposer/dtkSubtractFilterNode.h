// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractSubtractFilter;
class dtkSubtractFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkSubtractFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkSubtractFilterNode : public dtkComposerNodeObject<dtkAbstractSubtractFilter>
{
public:
     dtkSubtractFilterNode(void);
    ~dtkSubtractFilterNode(void);

public:
    void run(void);

private:
    dtkSubtractFilterNodePrivate *d;
};

//
// dtkSubtractFilterNode.h ends here
