// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractImageMeshMapping;
class dtkImageMeshMappingNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkImageMeshMappingNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageMeshMappingNode : public dtkComposerNodeObject<dtkAbstractImageMeshMapping>
{
public:
     dtkImageMeshMappingNode(void);
    ~dtkImageMeshMappingNode(void);

public:
    void run(void);

private:
    dtkImageMeshMappingNodePrivate *d;
};

//
// dtkImageMeshMappingNode.h ends here
