// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractMeshICP;
class dtkMeshICPNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkMeshICPNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkMeshICPNode : public dtkComposerNodeObject<dtkAbstractMeshICP>
{
public:
     dtkMeshICPNode(void);
    ~dtkMeshICPNode(void);

public:
    void run(void);

private:
    dtkMeshICPNodePrivate *d;
};

//
// dtkMeshICPNode.h ends here
