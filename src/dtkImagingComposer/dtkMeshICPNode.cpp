// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshICPNode.h"

#include "dtkAbstractMeshICP.h"
#include "dtkImaging.h"
#include "dtkImage.h"
#include "dtkImageMesh.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkMeshICPNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkMeshICPNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImageMesh *> movingMesh;
    dtkComposerTransmitterReceiver<dtkImageMesh *> fixedMesh;
    dtkComposerTransmitterReceiver<int> mode;

    dtkComposerTransmitterEmitter<dtkImageMesh *> resMesh;

};

// /////////////////////////////////////////////////////////////////
// dtkMeshICPNode
// /////////////////////////////////////////////////////////////////

dtkMeshICPNode::dtkMeshICPNode(void) : dtkComposerNodeObject<dtkAbstractMeshICP>(), d(new dtkMeshICPNodePrivate())
{
    this->setFactory(dtkImaging::filters::meshICP::pluginFactory());

    this->appendReceiver(&d->movingMesh);
    this->appendReceiver(&d->fixedMesh);
    this->appendReceiver(&d->mode);

    this->appendEmitter (&d->resMesh);
}

dtkMeshICPNode::~dtkMeshICPNode(void)
{
    delete d;
}

void dtkMeshICPNode::run(void)
{
    if (d->movingMesh.isEmpty() || d->fixedMesh.isEmpty() || d->mode.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractMeshICP *filter = this->object();
        if(!filter) {
            dtkError() << Q_FUNC_INFO << "No Mesh ICP filter found. Aborting.";
            return;
        }

        filter->setMovingMesh(d->movingMesh.data());
        filter->setFixedMesh(d->fixedMesh.data());
        filter->setMode(d->mode.data());
        filter->run();
        d->resMesh.setData(filter->resMesh());
    }
}

//
// dtkMeshICPNode.cpp ends here
