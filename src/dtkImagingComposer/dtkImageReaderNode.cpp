// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageReaderNode.h"

#include <dtkImageReader.h>
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImageReaderNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkImageReaderNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString> pathRecv;

    dtkComposerTransmitterEmitter<dtkImage *> outTypeEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkImageReaderNode
// /////////////////////////////////////////////////////////////////

dtkImageReaderNode::dtkImageReaderNode(void) : dtkComposerNodeObject<dtkImageReader>(), d(new dtkImageReaderNodePrivate())
{
    this->setFactory(dtkImaging::reader::pluginFactory());

    this->appendReceiver(&d->pathRecv);
    this->appendEmitter(&d->outTypeEmt);
}

dtkImageReaderNode::~dtkImageReaderNode(void)
{
    delete d;
}

void dtkImageReaderNode::run(void)
{
    if (d->pathRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkImageReader *reader = this->object();

        if (!reader && this->implementations().empty()) {
            dtkError() << Q_FUNC_INFO << "No Image reader found. Aborting.";
        }

        QString path = d->pathRecv.constData();

        dtkImage *image = nullptr;

        if (reader) {
            image = reader->read(path);
        } else {
            image = dtkImaging::read(path);
        }

        if (!image) {
            dtkError() << Q_FUNC_INFO << "No Image read. Aborting.";
            return;
        }

        dtkObjectManager *om = dtkImaging::manager().objectManager();
        if (om) {
            om->add(dtkMetaType::variantFromValue(image), path);
        }

        d->outTypeEmt.setData(image);
    }
}

//
// dtkImageReaderNode.cpp ends here
