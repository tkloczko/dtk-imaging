// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractMultiplyFilter;
class dtkMultiplyFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkMultiplyFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkMultiplyFilterNode : public dtkComposerNodeObject<dtkAbstractMultiplyFilter>
{
public:
     dtkMultiplyFilterNode(void);
    ~dtkMultiplyFilterNode(void);

public:
    void run(void);

private:
    dtkMultiplyFilterNodePrivate *d;
};

//
// dtkMultiplyFilterNode.h ends here
