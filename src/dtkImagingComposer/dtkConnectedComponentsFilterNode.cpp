// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkConnectedComponentsFilterNode.h"

#include "dtkAbstractConnectedComponentsFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkConnectedComponentsFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkConnectedComponentsFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkConnectedComponentsFilterNode
// /////////////////////////////////////////////////////////////////

dtkConnectedComponentsFilterNode::dtkConnectedComponentsFilterNode(void) : dtkComposerNodeObject<dtkAbstractConnectedComponentsFilter>(), d(new dtkConnectedComponentsFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::connectedComponents::pluginFactory());

    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkConnectedComponentsFilterNode::~dtkConnectedComponentsFilterNode(void)
{
    delete d;
}

void dtkConnectedComponentsFilterNode::run(void)
{
    if (d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractConnectedComponentsFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Connected Components filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkConnectedComponentsFilterNode.cpp ends here
