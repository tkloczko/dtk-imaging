// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractErodeFilter;
class dtkErodeFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkErodeFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkErodeFilterNode : public dtkComposerNodeObject<dtkAbstractErodeFilter>
{
public:
     dtkErodeFilterNode(void);
    ~dtkErodeFilterNode(void);

public:
    void run(void);

private:
    dtkErodeFilterNodePrivate *d;
};

//
// dtkErodeFilterNode.h ends here
