// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageStatisticsNode.h"

#include "dtkAbstractImageStatistics.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImageStatisticsNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkImageStatisticsNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<double>      meanEmt;
    dtkComposerTransmitterEmitter<double>      sigmaEmt;
    dtkComposerTransmitterEmitter<double>      varianceEmt;
    dtkComposerTransmitterEmitter<double>      sumEmt;
    dtkComposerTransmitterEmitter<double>      minEmt;
    dtkComposerTransmitterEmitter<double>      maxEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkImageStatisticsNode
// /////////////////////////////////////////////////////////////////

dtkImageStatisticsNode::dtkImageStatisticsNode(void) : dtkComposerNodeObject<dtkAbstractImageStatistics>(), d(new dtkImageStatisticsNodePrivate())
{
    this->setFactory(dtkImaging::statistics::pluginFactory());
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter(&d->meanEmt);
    this->appendEmitter(&d->sigmaEmt);
    this->appendEmitter(&d->varianceEmt);
    this->appendEmitter(&d->sumEmt);

    this->appendEmitter(&d->minEmt);
    this->appendEmitter(&d->maxEmt);
}

dtkImageStatisticsNode::~dtkImageStatisticsNode(void)
{
    delete d;
}

void dtkImageStatisticsNode::run(void)
{
    if (d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        dtkAbstractImageStatistics *statistics = this->object();

        if (!statistics) {
            dtkError() << Q_FUNC_INFO << "No Statistics calculator found. Aborting.";
            return;
        }

        statistics->setImage(d->imgRecv.constData());
        statistics->run();

        d->meanEmt.setData(statistics->mean());
        d->sigmaEmt.setData(statistics->sigma());
        d->varianceEmt.setData(statistics->variance());
        d->sumEmt.setData(statistics->sum());

        d->minEmt.setData(statistics->min());
        d->maxEmt.setData(statistics->max());
    }
}

//
// dtkImageStatisticsNode.cpp ends here
