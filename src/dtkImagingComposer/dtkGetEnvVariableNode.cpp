// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkGetEnvVariableNode.h"

#include <dtkLog>

Q_DECLARE_METATYPE(QProcessEnvironment);

// /////////////////////////////////////////////////////////////////
// dtkGetEnvVariableNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkGetEnvVariableNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString>             nameRecv;
    dtkComposerTransmitterReceiver<QProcessEnvironment> envRecv;

    dtkComposerTransmitterEmitter<QString> valueEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkGetEnvVariableNode
// /////////////////////////////////////////////////////////////////

dtkGetEnvVariableNode::dtkGetEnvVariableNode(void) : d(new dtkGetEnvVariableNodePrivate())
{
    this->appendReceiver(&d->nameRecv);
    this->appendReceiver(&d->envRecv);

    this->appendEmitter(&d->valueEmt);
}

dtkGetEnvVariableNode::~dtkGetEnvVariableNode(void)
{
    delete d;
}

void dtkGetEnvVariableNode::run(void)
{
    if (d->nameRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input \"name\" is not set. Aborting.";
        return;
    }

    if (d->envRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input \"env\" is not set. Aborting.";
        return;
    }
    d->valueEmt.setData(d->envRecv.data().value(d->nameRecv.data()));
}

//
// dtkGetEnvVariableNode.cpp ends here
