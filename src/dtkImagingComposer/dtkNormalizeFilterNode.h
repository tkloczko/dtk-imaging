// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractNormalizeFilter;
class dtkNormalizeFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkNormalizeFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkNormalizeFilterNode : public dtkComposerNodeObject<dtkAbstractNormalizeFilter>
{
public:
     dtkNormalizeFilterNode(void);
    ~dtkNormalizeFilterNode(void);

public:
    void run(void);

private:
    dtkNormalizeFilterNodePrivate *d;
};

//
// dtkNormalizeFilterNode.h ends here
