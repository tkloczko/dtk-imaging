// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkImageWriter;
class dtkImageWriterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkImageWriterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageWriterNode : public dtkComposerNodeObject<dtkImageWriter>
{
public:
     dtkImageWriterNode(void);
    ~dtkImageWriterNode(void);

public:
    void run(void);

private:
    dtkImageWriterNodePrivate *d;
};

//
// dtkImageWriterNode.h ends here
