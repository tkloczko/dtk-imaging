// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMultiplyFilterNode.h"

#include "dtkAbstractMultiplyFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkMultiplyFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkMultiplyFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkMultiplyFilterNode
// /////////////////////////////////////////////////////////////////

dtkMultiplyFilterNode::dtkMultiplyFilterNode(void) : dtkComposerNodeObject<dtkAbstractMultiplyFilter>(), d(new dtkMultiplyFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::multiply::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkMultiplyFilterNode::~dtkMultiplyFilterNode(void)
{
    delete d;
}

void dtkMultiplyFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractMultiplyFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Multiply filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->setValue(d->radiusRecv.constData());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkMultiplyFilterNode.cpp ends here
