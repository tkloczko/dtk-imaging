// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkHTransformFilterNode.h"

#include "dtkAbstractHTransformFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkHTransformFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkHTransformFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     h_value;
    dtkComposerTransmitterReceiver<dtkImage *> image_in;

    dtkComposerTransmitterEmitter<dtkImage *>  image_out;
};

// /////////////////////////////////////////////////////////////////
// dtkHTransformFilterNode
// /////////////////////////////////////////////////////////////////

dtkHTransformFilterNode::dtkHTransformFilterNode(void) : dtkComposerNodeObject<dtkAbstractHTransformFilter>(), d(new dtkHTransformFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::htransform::pluginFactory());

    this->appendReceiver(&d->image_in);
    this->appendReceiver(&d->h_value);

    this->appendEmitter (&d->image_out);
}

dtkHTransformFilterNode::~dtkHTransformFilterNode(void)
{
    delete d;
}

void dtkHTransformFilterNode::run(void)
{
    if (d->h_value.isEmpty() || d->image_in.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractHTransformFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No HTransform filter found. Aborting.";
            return;
        }
        filter->setImage(d->image_in.data());
        filter->setHValue(d->h_value.constData());
        filter->run();
        d->image_out.setData(filter->filteredImage());
    }
}

//
// dtkHTransformFilterNode.cpp ends here
