// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMaskApplicationFilterNode.h"

#include "dtkAbstractMaskApplicationFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkMaskApplicationFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkMaskApplicationFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     bgdValue;
    dtkComposerTransmitterReceiver<dtkImage *> image;
    dtkComposerTransmitterReceiver<dtkImage *> mask;

    dtkComposerTransmitterEmitter<dtkImage *>  res;
};

// /////////////////////////////////////////////////////////////////
// dtkMaskApplicationFilterNode
// /////////////////////////////////////////////////////////////////

dtkMaskApplicationFilterNode::dtkMaskApplicationFilterNode(void) : dtkComposerNodeObject<dtkAbstractMaskApplicationFilter>(), d(new dtkMaskApplicationFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::maskApplication::pluginFactory());

    this->appendReceiver(&d->image);
    this->appendReceiver(&d->mask);
    this->appendReceiver(&d->bgdValue);

    this->appendEmitter (&d->res);
}

dtkMaskApplicationFilterNode::~dtkMaskApplicationFilterNode(void)
{
    delete d;
}

void dtkMaskApplicationFilterNode::run(void)
{
    if (d->mask.isEmpty() || d->image.isEmpty() || d->bgdValue.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractMaskApplicationFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Mask application filter found. Aborting.";
            return;
        }
        filter->setImage(d->image.data());
        filter->setMask(d->mask.constData());
        filter->setBackgroundValue(d->bgdValue.data());
        filter->run();
        d->res.setData(filter->result());
    }
}

//
// dtkMaskApplicationFilterNode.cpp ends here
