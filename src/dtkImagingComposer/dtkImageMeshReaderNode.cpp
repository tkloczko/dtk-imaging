// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageMeshReaderNode.h"

#include "dtkImageMeshReader.h"
#include "dtkImageMesh.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImageMeshReaderNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkImageMeshReaderNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString>       pathRecv;
    dtkComposerTransmitterEmitter<dtkImageMesh *> meshEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkImageMeshReaderNode
// /////////////////////////////////////////////////////////////////

dtkImageMeshReaderNode::dtkImageMeshReaderNode(void) : d(new dtkImageMeshReaderNodePrivate())
{
    this->setFactory(dtkImaging::meshReader::pluginFactory());

    this->appendReceiver(&d->pathRecv);
    this->appendEmitter(&d->meshEmt);
}

dtkImageMeshReaderNode::~dtkImageMeshReaderNode(void)
{
    delete d;
}

void dtkImageMeshReaderNode::run(void)
{
    if (d->pathRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        dtkImageMeshReader *reader = this->object();
        if (!reader) {
            dtkError() << Q_FUNC_INFO << "No Mesh reader found. Aborting.";
            return;
        }
        dtkImageMesh *mesh = reader->read(d->pathRecv.data());

        if (!mesh) {
            dtkError() << "no mesh to read. Aborting.";
            return;
        }
        d->meshEmt.setData(mesh);
    }
}

//
// dtkImageMeshReaderNode.cpp ends here
