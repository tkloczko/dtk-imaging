// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSubtractionImageNode.h"

#include "dtkAbstractImageSubtraction.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkSubtractionImageNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkSubtractionImageNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv1;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv2;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkSubtractionImageNode
// /////////////////////////////////////////////////////////////////

dtkSubtractionImageNode::dtkSubtractionImageNode(void) : dtkComposerNodeObject<dtkAbstractImageSubtraction>(), d(new dtkSubtractionImageNodePrivate())
{
    this->setFactory(dtkImaging::filters::imageSubtraction::pluginFactory());

    this->appendReceiver(&d->imgRecv1);
    this->appendReceiver(&d->imgRecv2);

    this->appendEmitter (&d->imgEmt);
}

dtkSubtractionImageNode::~dtkSubtractionImageNode(void)
{
    delete d;
}

void dtkSubtractionImageNode::run(void)
{
    if (d->imgRecv2.isEmpty() || d->imgRecv1.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        dtkAbstractImageSubtraction *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Image Subtraction found. Aborting.";
            return;
        }
        filter->setLhs(d->imgRecv1.data());
        filter->setRhs(d->imgRecv2.data());
        filter->run();
        d->imgEmt.setData(filter->result());
    }
}

//
// dtkSubtractionImageNode.cpp ends here
