// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkRobexNode.h"

#include <QEventLoop>
#include <QFileInfo>
#include <QDir>
#include <QProcess>

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkRobexNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkRobexNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString> pathToROBEXExeRecv;
    dtkComposerTransmitterReceiver<QString> pathToROBEXDataRecv;
    dtkComposerTransmitterReceiver<QString> fileInRecv;
    dtkComposerTransmitterReceiver<QString> fileOutRecv;
    dtkComposerTransmitterReceiver<QString> maskInRecv;
    dtkComposerTransmitterReceiver<int>     seedRecv;

    dtkComposerTransmitterEmitter<QString>  fileOutEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkRobexNode
// /////////////////////////////////////////////////////////////////

dtkRobexNode::dtkRobexNode(void) : d(new dtkRobexNodePrivate())
{
    this->appendReceiver(&d->pathToROBEXExeRecv);
    this->appendReceiver(&d->pathToROBEXDataRecv);
    this->appendReceiver(&d->fileInRecv);
    this->appendReceiver(&d->fileOutRecv);
    this->appendReceiver(&d->maskInRecv);
    this->appendReceiver(&d->seedRecv);

    this->appendEmitter(&d->fileOutEmt);
}

dtkRobexNode::~dtkRobexNode(void)
{
    delete d;
}

void dtkRobexNode::run(void)
{
    //check mandatory inputs
    if (d->pathToROBEXExeRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The path to ROBEX executable is not set. Aborting.";
        return;
    }
    QFileInfo robexExe(d->pathToROBEXExeRecv.data());
    if(!robexExe.exists()) {
        dtkError() << Q_FUNC_INFO << "The path to ROBEX executable is not valid. Aborting.";
        return;
    }

    if (d->pathToROBEXDataRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The path to ROBEX data is not set. Aborting.";
        return;
    }
    QFileInfo robexData(d->pathToROBEXDataRecv.data());
    if(!robexData.exists()) {
        dtkError() << Q_FUNC_INFO << "The path to ROBEX executable is not valid. Aborting.";
        return;
    }

    if (d->fileInRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input file is not set. Aborting.";
        return;
    }

    if (d->fileOutRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The output file is not set. Aborting.";
        return;
    }
    //all mandatory inputs are set correctly

    QStringList args;
    args << d->fileInRecv.data() << d->fileOutRecv.data();

    //handle optional inputs if they are set:
    if (!d->maskInRecv.isEmpty()) {
        args << d->maskInRecv.data();
    }

    if (!d->seedRecv.isEmpty()) {
        args << QString::number(d->seedRecv.data());
    }

    //save path
    QString current = QDir::currentPath();
    QDir::setCurrent(d->pathToROBEXDataRecv.data());

    //call ROBEX as an external process, and wait for it to finish
    QProcess robexProcess;
    robexProcess.setProgram(d->pathToROBEXExeRecv.data());
    robexProcess.setArguments(args);
    QEventLoop el;
    QObject::connect(&robexProcess, SIGNAL(finished(int)), &el, SLOT(quit()));

    robexProcess.start();
    el.exec();

    dtkDebug() << robexProcess.readAllStandardError();
    dtkDebug() << robexProcess.readAllStandardOutput();

    //restore path
    QDir::setCurrent(current);
    //set output
    d->fileOutEmt.setData(d->fileOutRecv.data());
}


//
// dtkRobexNode.cpp ends here
