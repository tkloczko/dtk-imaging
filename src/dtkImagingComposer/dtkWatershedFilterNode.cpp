// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWatershedFilterNode.h"

#include "dtkAbstractWatershedFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkWatershedFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkWatershedFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> image_in;
    dtkComposerTransmitterReceiver<dtkImage *> seed;
    dtkComposerTransmitterReceiver<QString> control;

    dtkComposerTransmitterEmitter<dtkImage *>  image_out;
};

// /////////////////////////////////////////////////////////////////
// dtkWatershedFilterNode
// /////////////////////////////////////////////////////////////////

dtkWatershedFilterNode::dtkWatershedFilterNode(void) : dtkComposerNodeObject<dtkAbstractWatershedFilter>(), d(new dtkWatershedFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::watershed::pluginFactory());

    this->appendReceiver(&d->image_in);
    this->appendReceiver(&d->seed);
    this->appendReceiver(&d->control);

    this->appendEmitter (&d->image_out);
}

dtkWatershedFilterNode::~dtkWatershedFilterNode(void)
{
    delete d;
}

void dtkWatershedFilterNode::run(void)
{
    if ((d->image_in.isEmpty() || d->seed.isEmpty()) || d->control.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractWatershedFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Watershed filter found. Aborting.";
            return;
        }
        filter->setImage(d->image_in.data());
        filter->setSeed(d->seed.data());
        filter->setControlParameter(d->control.data());

        filter->run();

        d->image_out.setData(filter->filteredImage());
    }
}

//
// dtkWatershedFilterNode.cpp ends here
