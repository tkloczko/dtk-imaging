// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkImageMeshReader;
class dtkImageMeshReaderNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkImageMeshReaderNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageMeshReaderNode : public dtkComposerNodeObject<dtkImageMeshReader>
{
public:
     dtkImageMeshReaderNode(void);
    ~dtkImageMeshReaderNode(void);

public:
    void run(void);

private:
    dtkImageMeshReaderNodePrivate *d;
};

//
// dtkImageMeshReaderNode.h ends here
