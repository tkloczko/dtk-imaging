// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractConnectivityThresholding interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractConnectivityThreshold : public QRunnable
{
public:
    virtual void setMinSize(double minSize) = 0;
    virtual void setImage(dtkImage *image) = 0;

public:
    virtual dtkImage *resImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractConnectivityThreshold *)
DTK_DECLARE_PLUGIN        (dtkAbstractConnectivityThreshold, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractConnectivityThreshold, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractConnectivityThreshold, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractConnectivityThreshold, DTKIMAGINGFILTERS_EXPORT, connectivityThreshold);
    }
}

//
// dtkAbstractConnectivityThreshold.h ends here
