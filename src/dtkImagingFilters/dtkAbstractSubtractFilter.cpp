// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractSubtractFilter.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractSubtractFilter, subtract, dtkImaging);
    }
}

//
// dtkAbstractSubtractFilter.cpp ends here
