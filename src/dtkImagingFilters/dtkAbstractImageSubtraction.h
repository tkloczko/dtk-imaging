// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractImageSubtraction interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractImageSubtraction : public QRunnable
{
public:
    virtual void setLhs(dtkImage *image) = 0;
    virtual void setRhs(dtkImage *image) = 0;

public:
    virtual dtkImage *result(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractImageSubtraction *)
DTK_DECLARE_PLUGIN        (dtkAbstractImageSubtraction, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractImageSubtraction, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractImageSubtraction, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractImageSubtraction, DTKIMAGINGFILTERS_EXPORT, imageSubtraction);
    }
}

//
// dtkAbstractImageSubtraction.h ends here
