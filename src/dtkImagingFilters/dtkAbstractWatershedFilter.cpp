// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImaging.h"
#include "dtkAbstractWatershedFilter.h"

// ///////////////////////////////////////////////////////////////////
// Register to the layer
// ///////////////////////////////////////////////////////////////////

namespace dtkImaging {

    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractWatershedFilter, watershed, dtkImaging);
    }
}


//
// dtkAbstractWaterShedFilter.cpp ends here
