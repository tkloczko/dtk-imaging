// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractDilateFiltering interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractDilateFilter : public QRunnable
{
public:
    virtual void setRadius(double radius) = 0;
    virtual void setImage(dtkImage *image) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractDilateFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractDilateFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractDilateFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractDilateFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractDilateFilter, DTKIMAGINGFILTERS_EXPORT, dilate);
    }
}

//
// dtkAbstractDilateFilter.h ends here
