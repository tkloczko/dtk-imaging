// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractMeshICP.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractMeshICP, meshICP, dtkImaging);
    }
}

//
// dtkAbstractMeshICP.cpp ends here
