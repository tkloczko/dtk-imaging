// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;
class dtkImageMesh;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractMeshICP process interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractMeshICP : public QRunnable
{
public:
    virtual void setMovingMesh(dtkImageMesh *movingMesh) = 0;
    virtual void setFixedMesh(dtkImageMesh *fixedMesh) = 0;
    virtual void setMode(int mode) = 0;

public:
    virtual dtkImageMesh *resMesh(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractMeshICP *)
DTK_DECLARE_PLUGIN        (dtkAbstractMeshICP, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractMeshICP, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractMeshICP, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractMeshICP, DTKIMAGINGFILTERS_EXPORT, meshICP);
    }
}

//
// dtkAbstractMeshICP.h ends here
