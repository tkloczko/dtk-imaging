// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImaging.h"

#include "dtkAbstractHTransformFilter.h"

// ///////////////////////////////////////////////////////////////////
// Register to the layer
// ///////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractHTransformFilter, htransform, dtkImaging);
    }
}


//
// dtkAbstractHTransformFilter.cpp ends here
