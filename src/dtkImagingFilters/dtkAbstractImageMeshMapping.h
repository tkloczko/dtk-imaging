// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;
class dtkImageMesh;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractImageMeshMapping interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractImageMeshMapping : public QRunnable
{
public:
    virtual void setImage(dtkImage *image) = 0;
    virtual void setMesh(dtkImageMesh *mesh) = 0;

public:
    virtual dtkImageMesh *resMesh(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractImageMeshMapping *)
DTK_DECLARE_PLUGIN        (dtkAbstractImageMeshMapping, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractImageMeshMapping, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractImageMeshMapping, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractImageMeshMapping, DTKIMAGINGFILTERS_EXPORT, imageMeshMapping);
    }
}

//
// dtkAbstractImageMeshMapping.h ends here
