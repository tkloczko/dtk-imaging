#pragma once 

#include <QDebug>

#include "dtkPixel.h"

template <typename T> class dtkTemplatedPixelPrivate;


template <typename T, typename Enable=void> class dtkTemplatedPixel : public dtkPixel
{
public:

    typedef T PixelType;

    dtkTemplatedPixel();
    ~dtkTemplatedPixel();

    int pixelType() const;
};

template<typename T, typename Enable=void> QDebug operator<<(QDebug, dtkTemplatedPixel<T> const&);


#include "dtkTemplatedPixel.tpp"
