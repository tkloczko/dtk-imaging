#include "dtkTemplatedPixel.h"

template <typename T, typename Enable> dtkTemplatedPixel<T,Enable>::dtkTemplatedPixel()
{

}

template<typename T, typename Enable> dtkTemplatedPixel<T,Enable>::~dtkTemplatedPixel()
{

}

template <typename T, typename Enable> QDebug operator <<(QDebug debug, const dtkTemplatedPixel<T>& pix)
{
    debug << "pixel: type :"<< QMetaType::typeName(pix.pixelType()) <<"dimension :"<<pix.pixelDim();
    return debug;
}

template <typename T, typename Enable> int dtkTemplatedPixel<T,Enable>::pixelType() const
{
    return qMetaTypeId<T>();
}
