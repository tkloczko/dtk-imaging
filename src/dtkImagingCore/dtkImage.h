// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <QString>
#include <QObject>

#include <dtkImagingCoreExport.h>

class dtkImageData;
class dtkImagePrivate;

// /////////////////////////////////////////////////////////////////
// dtkImage interface
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImage : public QObject
{
    Q_OBJECT

public:
    dtkImage(QObject *parent = NULL);

    dtkImage(qlonglong x, qlonglong y, QObject *parent = NULL);
    dtkImage(qlonglong x, qlonglong y, qlonglong z, QObject *parent = NULL);

    dtkImage(const dtkImage& other);

public:
    virtual ~dtkImage(void);

public:
    dtkImage& operator = (const dtkImage& other);

public:
    int dim(void) const;
    int storageType(void) const;

public:
    dtkImageData *data(void) const;
    void *rawData(void) const;

public:
    void setData(dtkImageData*);

public:
    QString identifier(void) const;
    QString pixelId(void) const;

public:
    const int pixelDim(void) const;

public:
    const dtkArray<double>& origin(void) const;
    const dtkArray<double>& spacing(void) const;
    const dtkArray<qlonglong>& extent(void) const;
    const dtkArray<double>& transformMatrix(void) const;

public:
    qlonglong xDim(void) const;
    qlonglong yDim(void) const;
    qlonglong zDim(void) const;

public:
    qlonglong bufferSize(void) const;

public:
    void setOrigin(const dtkArray<double>& origin);
    void setSpacing(const dtkArray<double>& spacing);
    void setTransformMatrix(const dtkArray<double>& transformMatrix);

public:
    static dtkImage *fromRawData(int dim, int storage_type, qlonglong x_dim, qlonglong y_dim, qlonglong z_dim, double dx, double dy, double dz, void *raw_data);

private:
    dtkImagePrivate *d;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkImage)
DTK_DECLARE_OBJECT(dtkImage *)

//
// dtkImage.h ends here
