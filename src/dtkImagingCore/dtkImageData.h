// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <dtkCore>

#include <dtkImagingCoreExport.h>

// /////////////////////////////////////////////////////////////////
// dtkImageData interface
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageData : public QObject
{
public:
    dtkImageData(QObject *parent = NULL);
    dtkImageData(const dtkImageData& other);

public:
    virtual ~dtkImageData(void);

public:
    virtual int dim(void) const = 0;
    virtual int storageType(void) const = 0;
    virtual QString pixelId(void) const = 0;

public:
    virtual const int pixelDim(void) const = 0;

public:
    virtual void *rawData(void) const = 0;

public:
    virtual dtkImageData *clone(void) const = 0;

public:
    virtual QString identifier(void) const = 0;

public:
    virtual const dtkArray<double>& origin(void) const = 0;
    virtual const dtkArray<double>& spacing(void) const = 0;
    virtual const dtkArray<qlonglong>& extent(void) const = 0;
    virtual const dtkArray<double>& transformMatrix(void) const = 0;

    virtual qlonglong bufferSize(void) const = 0;

public:
    virtual void setOrigin(const dtkArray<double>& origin) = 0;
    virtual void setSpacing(const dtkArray<double>& spacing) = 0;
    virtual void setTransformMatrix(const dtkArray<double>& spacing) = 0;
};

// /////////////////////////////////////////////////////////////////
// Debug operators
// /////////////////////////////////////////////////////////////////

DTKIMAGINGCORE_EXPORT QDebug operator << (QDebug dbg, dtkImageData *image_data);

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkImageData *)
DTK_DECLARE_PLUGIN        (dtkImageData, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageData, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageData, DTKIMAGINGCORE_EXPORT)

//
// dtkImageData.h ends here
