// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <QtCore>

class DTKIMAGINGCORE_EXPORT dtkPixel
{

public:
    virtual int pixelDim(void) const = 0;
    virtual int pixelType(void) const = 0;
    virtual QString identifier(void) const = 0;
};

//
// dtkPixel.h ends here
