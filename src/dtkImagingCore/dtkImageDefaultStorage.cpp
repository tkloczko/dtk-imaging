// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
  \class dtkImageDefaultStorage
  \inmodule dtkImaging
  \brief The dtkImageDefaultStorage template class is the layer-specific image type.

   The dtkImageDefaultStorage template class stores a N-dimensional
   image, where the image data is stored as an array of T. This buffer
   may be accessed through the rawData() method.
*/


/*! \fn dtkImageDefaultStorage::dtkImageDefaultStorage(QObject *parent = NULL)

  Constructs an empty storage.

  If \a parent is non-zero, the storage becomes a child of the \a
  parent. It means that the new storage is deleted when its parents is
  deleted.

*/

/*! \fn dtkImageDefaultStorage::dtkImageDefaultStorage(qlonglong x, qlonglong y, typename T::PixelType *data = NULL, QObject *parent = NULL)

  Constructs a storage with \a x size and \a y size in the two first
  dimensions.

  If \a data pointer is not NULL, no memory is allocated, the \a data
  pointer is reused for the storage.

  If \a parent is non-zero, the storage becomes a child of the \a
  parent. It means that the new storage is deleted when its parents is
  deleted.
*/

/*! \fn dtkImageDefaultStorage::dtkImageDefaultStorage(qlonglong x, qlonglong y, qlonglong z, typename T::PixelType *data = NULL, QObject *parent = NULL)

  Constructs a storage with \a x size, \a y size and \a z size in the
  three first dimensions.

  If \a data pointer is not NULL, no memory is allocated, the \a data
  pointer is reused for the storage.

  If \a parent is non-zero, the storage becomes a child of the \a
  parent. It means that the new storage is deleted when its parents is
  deleted.
*/

/*! \fn dtkImageDefaultStorage::dtkImageDefaultStorage(dtkArray<qlonglong> dims, QObject *parent = NULL)

  Constructs a storage with dimension sizes given by the array \a
  dims.

  If \a parent is non-zero, the storage becomes a child of the \a
  parent. It means that the new storage is deleted when its parents is
  deleted.
*/

/*! \fn dtkImageDefaultStorage::dtkImageDefaultStorage(dtkArray<qlonglong> dimensions, typename T::PixelType *data = NULL, QObject *parent = NULL)

  Constructs a storage with dimension sizes given by the array \a
  dimensions.

  If \a data pointer is not NULL, no memory is allocated, the \a data
  pointer is reused for the storage.

  If \a parent is non-zero, the storage becomes a child of the \a
  parent. It means that the new storage is deleted when its parents is
  deleted.
*/

/*! \fn dtkImageDefaultStorage::dtkImageDefaultStorage(const dtkImageDefaultStorage& other)

  \internal
*/

/*! \fn dtkImageDefaultStorage::~dtkImageDefaultStorage(void)

  Destroys the storage.

 */

/*! \fn void *dtkImageDefaultStorage::rawData(void) const

  Returns the address of the first element in the storage.
*/

/*! \fn dtkImageDefaultStorage *dtkImageDefaultStorage::clone(void) const

  Returns a clone of the storage.
*/

/*! \fn QString dtkImageDefaultStorage::identifier(void) const

  Returns the identifier of the storage.
*/

/*! \fn QString dtkImageDefaultStorage::pixelId(void) const

  Returns the identifier for the pixel type.
*/

/*! \fn QString dtkImageDefaultStorage::pixelId(void) const

  Returns the pixel's dimension.
*/

/*! \fn dtkArray<double> dtkImageDefaultStorage::origin(void) const

  Returns the origin in physical coordinates space.

  \sa spacing, extent, transformMatrix
*/

/*! \fn dtkArray<double> dtkImageDefaultStorage::spacing(void) const

  Returns the spacing in physical coordinates space

  \sa origin, extent, transformMatrix
*/

/*! \fn dtkArray<double> dtkImageDefaultStorage::transformMatrix(void) const

  Returns the transform matrix in physical coordinates space

  \sa origin, extent, spacing
*/

/*! \fn dtkArray<qlonglong> dtkImageDefaultStorage::extent(void) const

  Returns the image size along each axis.

  \sa spacing, origin
*/

/*! \fn void dtkImageDefaultStorage::setOrigin(const dtkArray<double>& origin)

  Sets the physical origin of the image from the coordinates given
  by the array \a origin.

  \sa setSpacing, setTransformMatrix
*/

/*! \fn void dtkImageDefaultStorage::setSpacing(const dtkArray<double>& spacing)

  Sets the physical spacing of the image from the array \a spacing.

  \sa setOrigin, setTransformMatrix
 */

/*! \fn void dtkImageDefaultStorage::setTransformMatrix(const dtkArray<double>& transformMatrix)

  Sets the physical transformation matrix of the image from the array \a transformMatrix.

  \sa setOrigin, setSpacing
 */



//
// dtkImageDefaultStorage.cpp ends here
