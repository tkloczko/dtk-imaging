// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <dtkCore>

class dtkImage;

// /////////////////////////////////////////////////////////////////
// Layer methods declarations
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {

    DTKIMAGINGCORE_EXPORT dtkCoreLayerManager& manager(void);

    DTKIMAGINGCORE_EXPORT void activateObjectManager(void);

    DTKIMAGINGCORE_EXPORT void initialize(const QString& path = QString());
    DTKIMAGINGCORE_EXPORT void setVerboseLoading(bool b);
    DTKIMAGINGCORE_EXPORT void setAutoLoading(bool b);
    DTKIMAGINGCORE_EXPORT void uninitialize(void);

    DTKIMAGINGCORE_EXPORT dtkImage *read (const QString& path);
    DTKIMAGINGCORE_EXPORT bool      write(const QString& path, dtkImage *image);
}

//
// dtkImaging.h ends here
