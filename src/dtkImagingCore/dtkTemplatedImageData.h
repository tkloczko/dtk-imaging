// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkImageData.h"
#include "dtkTemplatedPixel.h"

// /////////////////////////////////////////////////////////////////
// dtkTemplatedImageData interface
// /////////////////////////////////////////////////////////////////

template <typename T = dtkTemplatedPixel<unsigned char>, unsigned int N = 3> class dtkTemplatedImageData : public dtkImageData
{
public:
    dtkTemplatedImageData(QObject *parent = NULL) : dtkImageData(parent) {}

public:
    int dim(void) const;
    int storageType(void) const;
};

// /////////////////////////////////////////////////////////////////
// dtkTemplatedImageData implementation
// /////////////////////////////////////////////////////////////////

template <typename T, unsigned int N> inline int dtkTemplatedImageData<T, N>::dim(void) const
{
    return N;
}

template <typename T, unsigned int N> inline int dtkTemplatedImageData<T, N>::storageType(void) const
{
    return qMetaTypeId<typename T::PixelType>();
}
//
// dtkTemplatedImageData.h ends here
