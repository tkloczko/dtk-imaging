// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <dtkCore>

#include "dtkTemplatedImageData.h"

template <typename T = dtkTemplatedPixel<unsigned char>, unsigned int N = 3> class dtkImageDefaultStoragePrivate;

// /////////////////////////////////////////////////////////////////
// dtkImageDefaultStorage interface
// /////////////////////////////////////////////////////////////////

template <typename T = dtkTemplatedPixel<unsigned char>, unsigned int N = 3> class dtkImageDefaultStorage : public dtkTemplatedImageData<T, N>
{
public:
    dtkImageDefaultStorage(QObject *parent = NULL);
    dtkImageDefaultStorage(qlonglong x, qlonglong y, typename T::PixelType *data = NULL, QObject *parent = NULL);
    dtkImageDefaultStorage(qlonglong x, qlonglong y, qlonglong z, typename T::PixelType *data = NULL, QObject *parent = NULL);
    dtkImageDefaultStorage(const dtkArray<qlonglong>& extent, QObject *parent = NULL);
    dtkImageDefaultStorage(const dtkArray<qlonglong>& extent, typename T::PixelType *data = NULL, QObject *parent = NULL);
    dtkImageDefaultStorage(const dtkImageDefaultStorage& other);

public:
    ~dtkImageDefaultStorage(void);

public:
    QString pixelId(void) const;

public:
    const int pixelDim(void) const;

public:
    void *rawData(void) const;

public:
    dtkImageDefaultStorage *clone(void) const;

public:
    QString identifier(void) const;

public:
    const dtkArray<double>& origin(void) const;
    const dtkArray<double>& spacing(void) const;
    const dtkArray<qlonglong>& extent(void) const;
    const dtkArray<double>& transformMatrix(void) const;

    qlonglong bufferSize(void) const;

public:
    void setOrigin(const dtkArray<double>& origin);
    void setSpacing(const dtkArray<double>& spacing);
    void setTransformMatrix(const dtkArray<double>& transformMatrix);

private:
    dtkImageDefaultStoragePrivate<T, N> *d;
    qlonglong initialize(const dtkArray<qlonglong>& extent, typename T::PixelType *data = NULL);
};

// /////////////////////////////////////////////////////////////////

#include "dtkImageDefaultStorage.tpp"

//
// dtkImageDefaultStorage.h ends here
