// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkPixelImpl.h"

#include <algorithm>

// /////////////////////////////////////////////////////////////////
// dtkImageDataITKPrivate interface
// /////////////////////////////////////////////////////////////////

template <typename T, unsigned int N> class dtkImageDefaultStoragePrivate
{
public:
  typename T::PixelType *m_image;
  dtkArray<qlonglong> m_extent;
  dtkArray<double> m_origin;
  dtkArray<double> m_transformMatrix;
  dtkArray<double> m_spacing;
};

// /////////////////////////////////////////////////////////////////
// dtkImageDefaultStorage implementation
// /////////////////////////////////////////////////////////////////

template <typename T, unsigned int N> inline qlonglong dtkImageDefaultStorage<T, N>::initialize(const dtkArray<qlonglong>& extent, typename T::PixelType *dataArray)
{
    if(extent.size() == 0) {
        qDebug() << Q_FUNC_INFO << __LINE__ << "0-dimension, storage not allocated";
        d->m_extent.resize(2*N);
        d->m_extent.fill(0);
        d->m_image = NULL;
        return 0;
    }

    if(extent.size() != 2*N) {
        qDebug() << Q_FUNC_INFO << __LINE__ << "dimensions mismatch, storage not allocated";
        d->m_extent.resize(2*N);
        d->m_extent.fill(0);
        d->m_image = NULL;
        return 0;
    }

    d->m_extent.resize(2*N);
    d->m_spacing.resize(N);
    d->m_origin.resize(N);
    d->m_transformMatrix.resize(N*N);

    qlonglong size = 1;

    for(unsigned int i = 0; i < N; ++i) {
        d->m_extent[i * 2] = extent[i * 2];
        d->m_extent[i * 2 + 1] = extent[i * 2 + 1];
        d->m_spacing[i] = 1.0;
        d->m_origin[i] = 0.0;
        size *= (extent[2 * i + 1] - extent[2 * i] + 1);

        for(unsigned int j = 0; j < N; ++j) {
            if (i == j) {
                d->m_transformMatrix[N*i+j] = 1.0;
            } else {
                d->m_transformMatrix[N*i+j] = 0.0;
            }
        }
    }
    size *= T::PixelDim;

    if(dataArray != NULL) {
        d->m_image = dataArray;
    } else {
        d->m_image = new typename T::PixelType[size];
    }

    return size;
}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::dtkImageDefaultStorage(QObject *parent) : dtkTemplatedImageData<T, N>(parent), d(new dtkImageDefaultStoragePrivate<T, N>)
{
    d->m_extent.resize(2*N);
    d->m_spacing.resize(N);
    d->m_origin.resize(N);
    d->m_transformMatrix.resize(N*N);

    for(unsigned int i = 0; i < N; ++i) {
        d->m_extent[i * 2] = 0;
        d->m_extent[i * 2 + 1] = 0;
        d->m_spacing[i] = 1.0;
        d->m_origin[i] = 0.0;

        for(unsigned int j = 0; j < N; ++j) {
            if (i == j) {
                d->m_transformMatrix[N * i + j] = 1.0;
            } else {
                d->m_transformMatrix[N * i + j] = 0.0;
            }
        }
    }
    d->image = NULL;
}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::dtkImageDefaultStorage(const dtkArray<qlonglong>& extent, QObject *parent) : dtkTemplatedImageData<T, N>(parent), d(new dtkImageDefaultStoragePrivate<T, N>)
{
    initialize(extent);
}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::dtkImageDefaultStorage(const dtkArray<qlonglong>& extent, typename T::PixelType *data, QObject *parent) : dtkTemplatedImageData<T, N>(parent), d(new dtkImageDefaultStoragePrivate<T, N>)
{
    initialize(extent, data);
}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::dtkImageDefaultStorage(qlonglong x, qlonglong y, typename T::PixelType *data, QObject *parent) : dtkTemplatedImageData<T, N>(parent), d(new dtkImageDefaultStoragePrivate<T, N>)
{
    dtkArray<qlonglong> extent(2*N, 0LL);
    extent[1] = x - 1 ;
    extent[3] = y - 1;
    initialize(extent, data);
}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::dtkImageDefaultStorage(qlonglong x, qlonglong y, qlonglong z, typename T::PixelType *data, QObject *parent) : dtkTemplatedImageData<T, N>(parent), d(new dtkImageDefaultStoragePrivate<T, N>)
{
    dtkArray<qlonglong> extent(2*N, 0LL);
    extent[1] = x - 1;
    extent[3] = y - 1;
    extent[5] = z - 1;
    initialize(extent, data);
}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::dtkImageDefaultStorage(const dtkImageDefaultStorage& other) : dtkTemplatedImageData<T, N>(NULL), d(new dtkImageDefaultStoragePrivate<T, N>)
{
    qlonglong size = initialize(other.extent());

    typename T::PixelType *src = other.d->m_image;
    typename T::PixelType *end = src + size;
    std::copy(src, end, d->m_image);

    this->setSpacing(other.spacing());
    this->setOrigin(other.origin());
    this->setTransformMatrix(other.transformMatrix());

}

template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N>::~dtkImageDefaultStorage(void)
{
    if (d->m_image) {
        delete[] d->m_image;
    }
}

template <typename T, unsigned int N> inline QString dtkImageDefaultStorage<T, N>::identifier(void) const
{
    return "default";
}

template <typename T, unsigned int N> inline QString dtkImageDefaultStorage<T, N>::pixelId(void) const
{
    return T::pixelId();
}

template <typename T, unsigned int N> inline const int dtkImageDefaultStorage<T, N>::pixelDim(void) const
{
    return T::PixelDim;
}


template <typename T, unsigned int N> inline dtkImageDefaultStorage<T, N> *dtkImageDefaultStorage<T, N>::clone(void) const
{
    return new dtkImageDefaultStorage<T, N>(*this);
}

template <typename T, unsigned int N> inline void *dtkImageDefaultStorage<T, N>::rawData(void) const
{
    return static_cast<void *>(d->m_image);
}

template <typename T, unsigned int N> inline qlonglong dtkImageDefaultStorage<T, N>::bufferSize(void) const
{
    if (N != 0) {
        qlonglong s = 1LL;
        for(unsigned int i = 0; i < N; ++i) {
            s *= (d->m_extent[2 * i + 1] - d->m_extent[2 * i] + 1);
        }
        return s;
    }

    return 0LL;
}

template <typename T, unsigned int N> inline const dtkArray<double>& dtkImageDefaultStorage<T, N>::origin(void) const
{
    return d->m_origin;
}

template <typename T, unsigned int N> inline const dtkArray<double>& dtkImageDefaultStorage<T, N>::spacing(void) const
{
    return d->m_spacing;
}

template <typename T, unsigned int N> inline const dtkArray<qlonglong>& dtkImageDefaultStorage<T, N>::extent(void) const
{
    return d->m_extent;
}

template <typename T, unsigned int N> inline const dtkArray<double>& dtkImageDefaultStorage<T, N>::transformMatrix(void) const
{
    return d->m_transformMatrix;
}

template <typename T, unsigned int N> inline void dtkImageDefaultStorage<T, N>::setSpacing(const dtkArray<double>& spacing)
{
    for(unsigned int i = 0; i < N; ++i) {
        d->m_spacing[i] = spacing.at(i);
    }
}

template <typename T, unsigned int N> inline void dtkImageDefaultStorage<T, N>::setOrigin(const dtkArray<double>& origin)
{
    for(unsigned int i = 0; i < N; ++i) {
        d->m_origin[i] = origin.at(i);
    }
}

template <typename T, unsigned int N> inline void dtkImageDefaultStorage<T, N>::setTransformMatrix(const dtkArray<double>& transformMatrix)
{
    for(unsigned int i = 0; i < N*N; ++i) {
        d->m_transformMatrix[i] = transformMatrix.at(i);
    }
}

/*!
  Operator used for serialization
 */
//template <typename T, unsigned int N> QDataStream& dtkImageDefaultStorage<T, N>::operator<<( QDataStream& stream, const dtkImage& image )
//{
//      stream<<extent()<<spacing()<<origin()<<transformMatrix()<<d->m_image;
//}

/*!
  Operator used for deserialization
 */
//template <typename T, unsigned int N> QDataStream& dtkImageDefaultStorage<T, N>::operator>>( QDataStream& stream, dtkImage& image )
//{
//      stream>>d->imageData;
//}



//
// dtkImageDefaultStorage.tpp ends here
