// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImagingSettings.h"

/*!
 * \brief dtkImagingSettings::dtkImagingSettings
 *
 * Constructs settings containing a path to plugins
 */
dtkImagingSettings::dtkImagingSettings(void) : QSettings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-imaging")
{
    this->beginGroup("imaging");
    if(!this->allKeys().contains("plugins"))
        this->setValue("plugins", QString());
    this->sync();
    this->endGroup();
}

dtkImagingSettings::~dtkImagingSettings(void)
{

}

//
// dtkImagingSettings.cpp ends here
