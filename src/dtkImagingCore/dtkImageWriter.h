// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include <dtkCore>

class dtkImage;

// /////////////////////////////////////////////////////////////////
// dtkImageWriter interface
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageWriter
{
public:
    virtual ~dtkImageWriter(void) = default;

public:
    virtual void setPath(const QString&) = 0;
    virtual void setImage(dtkImage *) = 0;

public:
    virtual void write(void) = 0;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkImageWriter *)
DTK_DECLARE_PLUGIN        (dtkImageWriter, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageWriter, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageWriter, DTKIMAGINGCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkImageWriter, DTKIMAGINGCORE_EXPORT, writer);
}

//
// dtkImageWriter.h ends here
