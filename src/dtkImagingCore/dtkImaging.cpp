// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImaging.h"

#include <dtkLog>

#include <dtkConfig.h>

#include "dtkImagingSettings.h"

#include "dtkImageReader.h"
#include "dtkImageWriter.h"

// /////////////////////////////////////////////////////////////////
// Layer methods implementations
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {

    DTK_DEFINE_LAYER_MANAGER;

    void activateObjectManager(void) {
        manager().setObjectManager(dtkObjectManager::instance());
    }

    void initialize(const QString& s) {

        QString path = s;
        if (path.isEmpty()) {
            dtkImagingSettings imaging_settings;
            imaging_settings.beginGroup("imaging");
            path = imaging_settings.value("plugins").toString();
            imaging_settings.endGroup();
            if (path.isEmpty()) {
                QString dtk_prefix = DTK_INSTALL_PREFIX;
                // ugly: remove null characters added by conda
                path = QDir::cleanPath(dtk_prefix.remove(QChar::Null) + "/plugins/dtkImaging");
                dtkDebug() << "no plugin path configured, use default:" << path ;
            }
        }

        manager().initialize(path);
    }

    void setVerboseLoading(bool b)
    {
        manager().setVerboseLoading(b);
    }

    void setAutoLoading(bool b)
    {
        manager().setAutoLoading(b);
    }

    void uninitialize(void)
    {
        manager().uninitialize();
    }

    dtkImage *read(const QString &path)
    {
        QStringList readers = reader::pluginFactory().keys();

        if (readers.isEmpty()) {
            dtkError() << Q_FUNC_INFO << "No reader has been found. Null pointer is returned.";
            return nullptr;
        }

        dtkImageReader *reader = reader::pluginFactory().create(readers.at(0));
        if (!reader) {
            dtkError() << Q_FUNC_INFO << readers.at(0) << "cannot be created. Null pointer is returned.";
            return nullptr;
        }

        dtkImage *img = reader->read(path);

        return img;
    }

    bool write(const QString &path, dtkImage *image)
    {
        QStringList writers = writer::pluginFactory().keys();

        if (writers.isEmpty()) {
            dtkError() << Q_FUNC_INFO << "No writer has been found. False is returned.";
            return false;
        }

        dtkImageWriter *writer = writer::pluginFactory().create(writers.at(0));
        if (!writer) {
            dtkError() << Q_FUNC_INFO << writers.at(0) << "cannot be created. False is returned.";
            return false;
        }
        writer->setImage(image);
        writer->setPath(path);
        writer->write();

        return true;
    }
}

//
// dtkImaging.cpp ends here
