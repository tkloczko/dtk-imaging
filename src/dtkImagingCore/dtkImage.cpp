// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImage.h"
#include "dtkImageData.h"
#include "dtkImageDefaultStorage.h"
#include "dtkPixelImpl.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImagePrivate declaration
// /////////////////////////////////////////////////////////////////

class dtkImagePrivate
{
public:
    dtkImageData *imageData;
};

// /////////////////////////////////////////////////////////////////
// dtkImage implementation
// /////////////////////////////////////////////////////////////////

/*!
  \class dtkImage
  \inmodule dtkImaging
  \brief The dtkImage class is the base class of dtkImaging API.

  dtkImage primary design is to provide an easy-to-use pointer on
  images. For convinience dtkImage provides some methods allowing to
  retrieve basic informations about the image.
*/

/*!
  Constructs an empty image.

  If \a parent is non-zero, this image becomes a child of the \a
  parent. It means that the new image is deleted when its parents is
  deleted.
 */
dtkImage::dtkImage(QObject *parent) : QObject(parent), d(new dtkImagePrivate())
{

}

/*!
  Constructs a deep copy of \a other.
*/
dtkImage::dtkImage(const dtkImage& other) : d(new dtkImagePrivate())
{
    d->imageData = other.d->imageData->clone();
}

/*!
  Replaces the current content of this image by a deep copy of \a
  other and returns a reference to this image.
*/
dtkImage& dtkImage::operator = (const dtkImage& other)
{
    if (this == &other || (d && other.d && d->imageData == other.d->imageData)) {
        return *this;
    }

    if (d) {
        if (d->imageData) {
            delete d->imageData;
        }
        delete d;
    }
    d = new dtkImagePrivate();
    d->imageData = other.d->imageData->clone();
    return *this;
}

/*!
  Constructs a new dtkImage with dimensions \a x and \a y.

  If \a parent is non-zero, this image becomes a child of the \a
  parent. It means that the new image is deleted when its parents is
  deleted.

  \overload dtkImage()
 */
dtkImage::dtkImage(qlonglong x, qlonglong y, QObject *parent) : QObject(parent), d(new dtkImagePrivate())
{
    d->imageData = new dtkImageDefaultStorage<dtkScalarPixel<unsigned char>, 2>(x, y);
}

/*!
  Constructs a new dtkImage with dimensions \a x, \a y and \a z.

  If \a parent is non-zero, this image becomes a child of the \a
  parent. It means that the new image is deleted when its parents is
  deleted.

  \overload dtkImage()
 */
dtkImage::dtkImage(qlonglong x, qlonglong y, qlonglong z, QObject *parent) : QObject(parent), d(new dtkImagePrivate())
{
    d->imageData = new dtkImageDefaultStorage<dtkScalarPixel<unsigned char>, 2>(x, y, z);
}

/*! \fn virtual dtkImage::~dtkImage(void)
  Destroys the image and its contents when existing.
*/
dtkImage::~dtkImage(void)
{
    if (d) {
        if (d->imageData) {
            delete d->imageData;
        }
        delete d;
    }
}

/*!
  Returns the image dimension.

  \sa extent
 */
int dtkImage::dim(void) const
{
    return d->imageData->dim();
}

/*!
  Returns the image storage type, as encoded by QMetaType.
 */
int dtkImage::storageType(void) const
{
    return d->imageData->storageType();
}

/*!
  Sets the content of this image from \a data.
 */
void dtkImage::setData(dtkImageData* data)
{
    d->imageData = data;
}

/*!
  Returns the content of the image.
 */
dtkImageData* dtkImage::data(void) const
{
    return d->imageData;
}

/*!
  Returns a pointer of the image buffer.
 */
void* dtkImage::rawData(void) const
{
    return d->imageData->rawData();
}


/*!
  Returns the identifier of the underlying storage scheme.
 */
QString dtkImage::identifier(void) const
{
    return d->imageData->identifier();
}

/*!
  Returns an identifier for the pixel type.
 */
QString dtkImage::pixelId(void) const
{
    return d->imageData->pixelId();
}

/*!
  Returns the pixelDimension for the pixel type.
 */
const int dtkImage::pixelDim(void) const
{
    return d->imageData->pixelDim();
}


/*!
  Returns the origin in physical coordinates space.

  \sa spacing, extent, transformMatrix
 */
const dtkArray<double>& dtkImage::origin(void) const
{
    return d->imageData->origin();
}

/*!
  Returns the spacing in physical coordinates space

  \sa origin, extent, transformMatrix
 */
const dtkArray<double>& dtkImage::spacing(void) const
{
    return d->imageData->spacing();
}

/*!
  Returns the transformation matrix in physical coordinates space

  \sa origin, extent, spacing
 */
const dtkArray<double>& dtkImage::transformMatrix(void) const
{
    return d->imageData->transformMatrix();
}

/*!
  Returns the image size along each axis.

  \sa spacing, origin, bufferSize
 */
const dtkArray<qlonglong>& dtkImage::extent(void) const
{
    return d->imageData->extent();
}

/*!
  Returns the image size along x axis.

  \sa extent, yDim, zDim
 */
qlonglong dtkImage::xDim(void) const
{
  if(extent().size()>=2)
    return extent().at(1)-extent().at(0)+1;
  return -1;
}

/*!
  Returns the image size along y axis.

  \sa extent, xDim, zDim
 */
qlonglong dtkImage::yDim(void) const
{
  if(extent().size()>=4)
    return extent().at(3)-extent().at(2)+1;
  return -1;
}

/*!
  Returns the image size along z axis.

  \sa extent, xDim, yDim
 */
qlonglong dtkImage::zDim(void) const
{
  if(extent().size()>=6)
    return extent().at(5)-extent().at(4)+1;
  return -1;
}

/*!
  Returns the buffer size.

  \sa extent
 */
qlonglong dtkImage::bufferSize(void) const
{
    return d->imageData->bufferSize();
}


/*!  Sets the physical origin of the image from the coordinates given
  by the array \a origin.

  \sa setSpacing, setExtent, setTransformationMatrix
 */
void dtkImage::setOrigin(const dtkArray<double> &origin)
{
    d->imageData->setOrigin(origin);
}

/*!
  Sets the physical spacing of the image from the array \a spacing.

  \sa setOrigin, setExtent, setTransformationMatrix
 */
void dtkImage::setSpacing(const dtkArray<double> &spacing)
{
    d->imageData->setSpacing(spacing);
}

/*!
  Sets the transformation matrix of the image from the array \a transformMatrix.

  \sa setOrigin, setExtent, setSpacing
 */
void dtkImage::setTransformMatrix(const dtkArray<double> &transformMatrix)
{
    d->imageData->setTransformMatrix(transformMatrix);
}

/*!
  Creates a new image from raw data.

  \a dim is the number of dimensions of the image.
  \a storage_type

 */
dtkImage *dtkImage::fromRawData(int dim, int storage_type, qlonglong x_dim, qlonglong y_dim, qlonglong z_dim, double dx, double dy, double dz, void *raw_data)
{
    dtkImage *img = nullptr;
    dtkImageData *img_data = nullptr;

    if (dim == 2) {
        switch (storage_type) {
        case QMetaType::Char:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 2>(x_dim, y_dim, reinterpret_cast<char *>(raw_data));
            break;

        case QMetaType::UChar:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned char>, 2>(x_dim, y_dim, reinterpret_cast<unsigned char *>(raw_data));
            break;

        case QMetaType::Short:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<short>, 2>(x_dim, y_dim, reinterpret_cast<short *>(raw_data));
            break;

        case QMetaType::UShort:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned short>, 2>(x_dim, y_dim, reinterpret_cast<unsigned short *>(raw_data));
            break;

        case QMetaType::Int:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<int>, 2>(x_dim, y_dim, reinterpret_cast<int *>(raw_data));
            break;

        case QMetaType::UInt:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned int>, 2>(x_dim, y_dim, reinterpret_cast<unsigned int *>(raw_data));
            break;

        case QMetaType::Long:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<long>, 2>(x_dim, y_dim, reinterpret_cast<long *>(raw_data));
            break;

        case QMetaType::ULong:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned long>, 2>(x_dim, y_dim, reinterpret_cast<unsigned long *>(raw_data));
            break;

        case QMetaType::Float:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<float>, 2>(x_dim, y_dim, reinterpret_cast<float *>(raw_data));
            break;

        case QMetaType::Double:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<double>, 2>(x_dim, y_dim, reinterpret_cast<double *>(raw_data));
            break;

        case QMetaType::UnknownType:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << storage_type << "is not handled. Nothing is done";
            img_data = nullptr;
            break;
        }

    } else if (dim == 3) {

        switch (storage_type) {
        case QMetaType::Char:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<char *>(raw_data));
            break;

        case QMetaType::UChar:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned char>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<unsigned char *>(raw_data));
            break;

        case QMetaType::Short:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<short>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<short *>(raw_data));
            break;

        case QMetaType::UShort:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned short>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<unsigned short *>(raw_data));
            break;

        case QMetaType::Int:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<int>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<int *>(raw_data));
            break;

        case QMetaType::UInt:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned int>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<unsigned int *>(raw_data));
            break;

        case QMetaType::Long:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<long>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<long *>(raw_data));
            break;

        case QMetaType::ULong:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned long>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<unsigned long *>(raw_data));
            break;

        case QMetaType::Float:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<float>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<float *>(raw_data));
            break;

        case QMetaType::Double:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<double>, 3>(x_dim, y_dim, z_dim, reinterpret_cast<double *>(raw_data));
            break;

        case QMetaType::UnknownType:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << storage_type << "is not handled. Nothing is done";
            img_data = nullptr;
            break;
        }

    } else {
        dtkWarn() << Q_FUNC_INFO << "Image of dimension " << dim << "is not handled. Nothing is done";
        img_data = nullptr;
    }

    if (img_data) {
        img = new dtkImage();
        img->setData(img_data);
        dtkArray<double> spacing(3);
        spacing[0] = dx;
        spacing[1] = dy;
        spacing[2] = dz;
        img->setSpacing(spacing);

    } else {
        dtkWarn() << Q_FUNC_INFO << "No image instanciated. Nullptr is returned.";
    }

    return img;
}

//
// dtkImage.cpp ends here
