// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkImage.h"
#include "dtkPixelImpl.h"

#include <dtkLog>

#include <QtCore>

#include <type_traits>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

#define INVOKE_EXEC_MACRO_(storageType, dim)                             \
    if (pixelType == "Scalar") {                                        \
        dtkExecutor<Filter, t>::template exec< dtkScalarPixel<storageType>, dim >(filter); \
                                                                        \
    } else if (pixelType == "RGB") {                                    \
        dtkExecutor<Filter, t>::template exec< dtkRGBPixel<storageType>, dim >(filter); \
                                                                        \
    } else if (pixelType == "RGBA") {                                   \
        dtkExecutor<Filter, t>::template exec< dtkRGBAPixel<storageType>, dim>(filter); \
                                                                        \
    } else {                                                            \
        dtkWarn() << Q_FUNC_INFO << "Pixel type " << pixelType << " is not handled by executor"; \
    }

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

#define FILL_DIM_MACRO_(storageType)         \
    if (dim == 1){                           \
        INVOKE_EXEC_MACRO_(storageType, 1);  \
                                             \
    } else if (dim==2) {                     \
        INVOKE_EXEC_MACRO_(storageType, 2);  \
                                             \
    } else if (dim == 3) {                   \
        INVOKE_EXEC_MACRO_(storageType, 3);  \
                                             \
    } else if (dim == 4) {                   \
        INVOKE_EXEC_MACRO_(storageType, 4);  \
                                             \
    } else {                                                            \
        dtkWarn() << Q_FUNC_INFO << "Dimension " << dim << " is not handled by executor."; \
    }

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

enum TypeConstraint {
    ALL,
    INTEGER_ONLY
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template <typename Filter, TypeConstraint = ALL> class dtkExecutor
{
public:
    template <typename ImgT, int dim> static typename std::enable_if< (dim > 1) && (ImgT::PixelDim == 1) >::type exec(Filter *filter)
    {
        filter->template exec<ImgT, dim>();
    }

    template <typename ImgT, int dim> static typename std::enable_if< (dim <= 1) || (ImgT::PixelDim != 1) >::type  exec(Filter *filter)
    {
        dtkDebug() << Q_FUNC_INFO << "Method not implemented.";

        if (ImgT::PixelDim != 1) {
            dtkDebug() << Q_FUNC_INFO << "Pixel Dimension must be 1";
        }

        if (dim <= 1) {
            dtkDebug() << Q_FUNC_INFO << "Image dimension must be 2 or more";
        }
    }
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template <typename Filter> class dtkExecutor <Filter, INTEGER_ONLY>
{
public:
    template <typename ImgT, int dim> static typename std::enable_if< (dim>1) && (ImgT::PixelDim == 1) && std::is_integral<typename ImgT::PixelType>::value >::type exec(Filter *filter)
    {
        filter->template exec<ImgT, dim>();
    }

    template <typename ImgT, int dim> static typename std::enable_if< (dim<=1) || (ImgT::PixelDim != 1) || !std::is_integral<typename ImgT::PixelType>::value >::type  exec(Filter *filter)
    {
        dtkDebug() << Q_FUNC_INFO << "Method not implemented.";

        if (ImgT::PixelDim != 1) {
            dtkDebug() << Q_FUNC_INFO << "Pixel Dimension must be 1";
        }

        if (!std::is_integral<typename ImgT::PixelType>::value) {
            dtkDebug() << Q_FUNC_INFO << "Pixel must have integral type";
        }

        if( dim <= 1) {
            dtkDebug() << Q_FUNC_INFO << "Image dimension must be 2 or more";
        }
    }
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template <typename Filter, TypeConstraint t=ALL> class dtkFilterExecutor
{
public:
     static void run(Filter *filter, dtkImage *img)
     {
         int dim = img->dim();
         int storageType = img->storageType();
         QString pixelType = img->pixelId();

         run(filter, dim, storageType, pixelType);
     }

     static void run(Filter *filter, int dim, int storageType, QString pixelType)
     {
         switch(storageType) {

         case QMetaType::UChar:
             FILL_DIM_MACRO_(unsigned char);
             break;

         case QMetaType::Char:
             FILL_DIM_MACRO_(char);
             break;

         case QMetaType::UShort:
             FILL_DIM_MACRO_(unsigned short);
             break;

         case QMetaType::Short:
             FILL_DIM_MACRO_(short);
             break;

         case QMetaType::UInt:
             FILL_DIM_MACRO_(unsigned int);
            break;

         case QMetaType::Int:
             FILL_DIM_MACRO_(int);
             break;

         case QMetaType::ULong:
             FILL_DIM_MACRO_(unsigned long);
             break;

         case QMetaType::Long:
             FILL_DIM_MACRO_(long);
            break;

         case QMetaType::Float:
             FILL_DIM_MACRO_(float);
            break;

         case QMetaType::Double:
             FILL_DIM_MACRO_(double);
             break;

         default:
             dtkWarn() << Q_FUNC_INFO << "Storage type" << storageType << "is not handled by executor.";
             break;
         }
     }
};

//
// dtkFilterExecutor.h ends here
