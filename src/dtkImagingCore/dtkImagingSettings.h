// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <QtCore>

class DTKIMAGINGCORE_EXPORT dtkImagingSettings : public QSettings
{
public:
     dtkImagingSettings(void);
    ~dtkImagingSettings(void);
};

//
// dtkImagingSettings.h ends here
