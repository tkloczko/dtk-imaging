# ChangLog
## version 1.2.0 - 2018-11-09
 * CMake version required 3.6
 * C++14 compilation
 * Abstract Segmentation modified to set level and threshold
## version 1.1.1 - 2018-11-09
 * fix missing destruction of the data in dtkImage
## version 1.1.0 - 2018-07-25
 * use DTK_PYTHON_INSTALL_PATH
 * add setAutoLoading in dtkImaging namespace
## version 1.0.5 - 2018-07-23
 * fix for conda and DTK_INSTALL_PREFIX
## version 1.0.4 - 2018-07-20
 * clean path when using DTK_INSTALL_PREFIX
## version 1.0.3 - 2018-03-14
 * fix bad install of dtkImagingWidgets headers
## version 1.0.2 - 2018-03-14
 * really fix INSTALL_RPATH for composer extension
## version 1.0.1 - 2018-03-09
- fix target properties for composer extension plugin
## version 1.0.0 - 2018-03-08
- add filters abstractions (ErosionFilter, WatershedFilter, HTransform)
- add python wrapping
- layer rearchitecturing: split into Filter, Core, Composer and Widgets components
