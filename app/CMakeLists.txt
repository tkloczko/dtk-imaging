## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

if(DTK_IMAGING_BUILD_WIDGETS)
  add_subdirectory(dtkImageStatistics)
  add_subdirectory(dtkImageViewer)
endif(DTK_IMAGING_BUILD_WIDGETS)

######################################################################
### CMakeLists.txt ends here
